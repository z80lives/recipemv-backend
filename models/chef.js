const mongoose = require("mongoose");

const Schema = mongoose.Schema;

/**
 * Chef
 - User
 - Rating
 - noDish
 - noRecipe
 - noVideo
 - Contact
 - Restuarant
 - Profile

 */
const chefSchema = new Schema({
    account: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    contact: {
        type: Schema.Types.ObjectId,
        ref: "Contact"
    },
    description: {type: String},
    rating: {type: Number},
    thumbnail: {type: String},
    coverImage: {type: String, required: false}
});

module.exports = mongoose.model("Chef", chefSchema);
