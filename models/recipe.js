const mongoose = require("mongoose");

const Schema = mongoose.Schema;

/**
Recipe
 - Name
 - User
 - Category
 - Rating
 **/

function getPrice(num){
    return (num/100).toFixed();
}

function setPrice(num){
    return num*100;
}

const recipeSchema = new Schema({
    name: {type: String},
    creator: {type: Schema.Types.ObjectId, ref: "User"},
    category: {type: String},
    rating: {type: Number},
    instruction: {type: String},
    pictures: [{type:String}],
    price: {type: Number, get: getPrice, set: setPrice}
});

module.exports = mongoose.model("Recipe", recipeSchema);