const mongoose = require("mongoose");

const Schema = mongoose.Schema;

//Restuarant name is same as the user name
const restaurantSchema = new Schema({
    description: {type: String, required: false},
    pictures: [{type:String, required:false}],
    thumbnail: {type: String, required: false},
    contact: {
        type: Schema.Types.ObjectId,
        ref:"Contact"
    },
    account: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    coverImage: {type: String, required: false}
});

module.exports = mongoose.model("Restaurant", restaurantSchema); 
