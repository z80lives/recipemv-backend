const mongoose = require("mongoose");

const Schema = mongoose.Schema;

/**
 * Profile
 - Introduction
 - Bio
 - Education
 - Experience
 */
const profileSchema = new Schema({
    introduction: {type: String},
    bio: {type: String},
    education: {type: String},
    experience: {type: String}
});

module.exports = mongoose.model("Profile", profileSchema);