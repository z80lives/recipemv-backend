const mongoose = require("mongoose");

const Schema = mongoose.Schema;

/**
 * Contact
    - Island
    - Phone Number
    - Website
    - Twitter
    - Facebook
    - Google+
    - Instagram
 */
const contactSchema = new Schema({
    island: {type: String},
    address: {type: String},
    phone_number: {type: String},
    website: {type: String},
    twitter: {type: String},
    facebook: {type: String},
    google_plus: {type: String},
    instagram: {type: String}
});

module.exports = mongoose.model("Contact", contactSchema);