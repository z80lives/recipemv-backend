const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    restaurant: {type: Schema.Types.ObjectId, ref: "Restaurant"},
    chef: {type: Schema.Types.ObjectId, ref: "Chef"},
    type: {type: String},
    url: {type: String}
    /*
    createdRestaurants: [
        {
            type: Schema.Types.ObjectId,
            ref: "Restaurant"
        }
    ]*/
},
{
    timestamps: true
}
);

module.exports = mongoose.model("User", userSchema);
