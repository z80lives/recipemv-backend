require('graphql-import-node/register');
const express = require("express");
const bodyParser = require("body-parser");
const graphqlExpress = require("express-graphql");
const mongoose = require("mongoose");

const { makeExecutableSchema } = require("graphql-tools");

const authenticate = require("./middleware/auth");

const schema = require("./gql/schema");
const resolvers = require("./gql/resolvers");

const app = express();

app.use(bodyParser.json({limit: "1mb"}));

//const restaurants = [];


app.use(authenticate);

/*
app.use("/api", graphqlExpress({
    schema: schema,
    rootValue: resolvers,
    graphiql: true
}));*/
//console.log(resolvers);

app.use("/api", graphqlExpress({
    schema: makeExecutableSchema({
        typeDefs: schema,
    }),
    rootValue: resolvers,
    graphiql: true
})
);


mongoose.connect("mongodb://127.0.0.1/recipemv-dev")
    .then( ()=> {
        app.listen(4000);
    })
    .catch(err => {
        console.log("MONGODB FAILED", err);
    });

