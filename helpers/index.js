const helpers = {
    validators: {
        validateRequest: (req) => {
            if(!req.isAuth){
                throw Error("Request not authenticated.");
            }
            if(!req.user_id){
                throw Error("Cannot find associated User ID");
            }
        },
        validatePrice: (req) => {
            if ( req.body.price ) {
                req.assert('price', 'Enter a price (numbers only)').regex(/^\d+(\.\d{2})?$/);
            }
        },
        checkLoggedIn: req => {
            if(!req.isAuth){
                throw Error("Please log in.");
            }
        },
        validateUser: (user) => {
            if(!user){
                throw new Error("User does not exist!");            
            }
        }
    },
    mergers: {
        removePassword: (data) => ({
            ...data,
            password: null
        })
    },
    cleaners: {        
        removeUndefined: (kmap) => {
            for(key in kmap){
                if(kmap[key]===undefined)
                    delete kmap[key];
            }
        }
    }
};

module.exports = helpers;