const User = require("../../models/user");
const Contact = require("../../models/contact");
const Chef = require("../../models/chef");
const bcrypt = require("bcryptjs");
const helpers = require("../../helpers");


const ChefResolver = {
    chefs: (_, req) => {
        return Chef.find()        
            .populate("contact")
            .populate("account")
            .catch(err =>{
                throw err;
            });
    },
    chef: (arg) => {
        const _id = arg.chefId;
        const isUserId = arg.isUserID;
	if(!isUserId){
            return Chef.findById(_id)
		.populate("contact")
		.populate("account");
	}
	else{
	    
	    return Chef.findOne({account: _id})
		.populate("contact")
		.populate("account");
	}
    },
    createChef: async (args) => {
        const {name, email, password, island, address, thumbnail, description} = args.chefInput;
        const user = await User.findOne({email: email});
        if(description === null){
            description = "";
        }

        if(user){
            throw new Error("User already exists.");
        }

        const hashedPassword = await bcrypt.hash(password, 12);
        
        const newUser = new User({
            email: email,
            password: hashedPassword,
            name: name,
            type:"chef"            
        });

        const result = await newUser.save();

        const newContact = new Contact({
            address: address,
            island: island
        });
        const contactResult = await newContact.save();

        const newChef = new Chef({
            description,
            account: result._id,
            contact: contactResult._id,
            thumbnail: thumbnail
        });

        const chef = await newChef.save();

        return helpers.mergers.removePassword(chef._doc);                
    },
    updateChef: async (args, req) => {
	const chefId = args.chefId;
	const chef = await Chef.findById(chefId);
	if(!chef)
	    throw new Error("Chef not found");
	
	const userInput = args.chefInput;
	const {address, island, phone_number} = userInput;
	const {thumbnail, coverImage} = userInput;
	const {name} = userInput;

	const newContact = {address, island, phone_number};
        const newProfile = {thumbnail, coverImage};
        const newCreator = {name};

        helpers.cleaners.removeUndefined(newContact);
        helpers.cleaners.removeUndefined(newProfile);
        helpers.cleaners.removeUndefined(newCreator);

	await Contact.updateOne({_id: chef.contact}, newContact);        
        await User.updateOne({_id: chef.account}, newCreator);
        await Chef.updateOne({_id: chef._id}, newProfile);

	const chef_updated = await Chef.findById(chefId) ;//.populate("account").populate("contact");
	return chef_updated;
    }
}

module.exports = ChefResolver;
