const bcrypt = require("bcryptjs");
const User = require("../../models/user");
const Chef = require("../../models/chef");
const jwt = require("jsonwebtoken");
const helpers = require("../../helpers");

const SECRET_KEY = "1234";

const AuthResolver = {
    user: async(_, req)=>{
        var user_type = "public";        
        helpers.validators.validateRequest(req);
        const user = await User.findOne({_id: req.user_id});
        
        helpers.validators.validateUser(user);

        if(user.restaurant && user.restaurant.id != null)
            user_type = "restaurant";
        else if(user.type && user.type=="chef"){
            user_type = "chef";
	    user._doc["chef"] = Chef.findOne({account: req.user_id});
	}
	
        user._doc["type"] = user_type;
        return helpers.mergers.removePassword(user._doc);
    },
    
    login: async (args) =>{
        const {email, password} = args;
        const user = await User.findOne({email: email});
        
        if(!user){
            throw new Error("User does not exit");
        }

        const result = await bcrypt.compare(password, user.password);

        if(!result){
            throw new Error("Password is incorrect");
        }

        const token = await jwt.sign({
            user_id: user.id,
            email: user.email
        },
        SECRET_KEY,
        {
            expiresIn: "1h"
        }
        );

        if(user.restaurant && user.restaurant.id != null)
            user_type = "restaurant";
        else if(user.type && user.type=="chef")
            user_type = "chef";


        return {
            user_id: user.id,
            token: token,
            token_expiration: 1,
            type: user_type
        }
    },

    refreshToken: async(args, req) => {
        helpers.validators.validateRequest(req);        

        const payload = jwt.verify(req.token, SECRET_KEY);    
        delete payload.iat;
        delete payload.exp;
        delete payload.nbf;
        delete payload.jti;
        //const jwtSignOptions = Object.assign({ }, this.options, { jwtid: refreshOptions.jwtid });
        const refreshPayload=  await jwt.sign(payload, SECRET_KEY,
            {
            expiresIn: "1h"
        });                             
        return {
            user_id: req.user_id,
            token: refreshPayload,
            token_expiration: 1
        };
    },

    getToken: async (_, req) => {
        helpers.validators.validateRequest(req);
        const decoded = jwt.decode(req.token, SECRET_KEY);            
        const d1 = new Date(decoded.iat);
        const d2 = new Date(decoded.exp);
        const now = Date.now()/1000;
        //const exp = new Date(new Date().getTime() + (d2-d1));
        const exp = Math.abs(now-decoded.exp)/60; //36e5;
        return {
            user_id: req.user_id,
            token: req.token,
            token_expiration: decoded.exp,
            token_issued: decoded.iat,
            remaining_time: exp+"h"
        }
    },    

    createUser: (args) => {            
        const {email, password} = args.userInput;

        return User.findOne({email: email})
            .then(user => {
                if(user){
                    throw new Error("User exists already");
                }
                return bcrypt.hash(password, 12)
            })           
            .then(hashedPassword => {
                const user = new User({
                    email: email,
                    password: hashedPassword
                });
                return user.save();
            })
            .then(result=>{
                return {...result._doc, password: null}
            })
            .catch(err => {
                throw err;
            });
    }
};
module.exports = AuthResolver;
