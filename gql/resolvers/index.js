const restaurantResolver = require("./restaurant");
const authResolver = require("./auth");
const recipeResolvers = require("./recipe");
const chefResolvers = require("./chef");
const helpers = require("../../helpers");

module.exports = 
{
    ...authResolver,
    ...restaurantResolver,
    ...recipeResolvers,
    ...chefResolvers
}