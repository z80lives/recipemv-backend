const helpers = require("../../helpers");
const Recipe = require("../../models/recipe");

const RecipeResolvers = {
    recipes: (params, req) => {        
        if(params.owner){
            helpers.validators.checkLoggedIn(req);
            const user_id = req.user_id;
            return Recipe.find({creator: user_id})
                .populate("creator");    
        }else if(params.id){            
            return [Recipe.findById(params.id)
                .populate("creator")];
        }else{
            return Recipe.find()
                .populate("creator");
                
        }
    },
    createRecipe: async (args, req) => {
        const {name, instruction, pictures} = args.recipeInput;        
        helpers.validators.checkLoggedIn(req);
        const user_id = req.user_id;
        const price = (args.recipeInput.price) || 0.0;
        const newRecipe = new Recipe({
            name: name,
            creator: user_id,
            instruction: instruction,
            category: "breakfast",
            rating: 0,
            price: price,
            pictures: pictures
        });
        const result = await newRecipe.save();
        return result._doc;
    },
    removeRecipe: async (args, req) => {
        const {recipeId} = args;
        console.log("Removing ", recipeId);
        helpers.validators.checkLoggedIn(req);
        const user_id = req.user_id;
        const target_recipe = await Recipe.findOne({_id: recipeId});
        if(target_recipe.creator != user_id){
            throw Error("You are not allowed to delete other user's recipes.");            
        }
        await target_recipe.remove();
        return true;
    }
};

module.exports = RecipeResolvers;