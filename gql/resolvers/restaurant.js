const Restaurant = require("../../models/restaurant");
const Contact = require("../../models/contact");
const bcrypt = require("bcryptjs");
const User = require("../../models/user");
const helpers = require("../../helpers");

const RestaurantResolvers = {
    restaurants: (_, req) => {
        /*if(!req.isAuth){
            throw Error("Request not authenticated.");
        }*/        
        return Restaurant.find()        
            .populate("contact")
            .populate("account")
            .then( restaurants => {
                return restaurants;
            })
            .catch(err =>{
                throw err;
            });
    },
    restaurant: (arg) => {
        const _id = arg.restaurantId;        
        return Restaurant.findById(_id)
            .populate("contact")
            .populate("account");
    },
    createRestaurant: async (args) =>{        
        const {name, email, address, description, password, island, thumbnail} = args.restaurantInput;
        if(description === null){
            description = "";
        }

        const user = await User.findOne({email: email});
        if(user){
            throw new Error("User already exists.");
        }

        const hashedPassword = await bcrypt.hash(password, 12);
        
        const newUser = new User({
            email: email,
            password: hashedPassword,
            name: name,
            type: "restaurant"
        });

        const result = await newUser.save();

        const newContact = new Contact({
            address: address,
            island: island
        });
        const contactResult = await newContact.save();

        const restaurant = new Restaurant({
            description: description,
            account: result._id,
            contact: contactResult._id,
            thumbnail: thumbnail
        });

        const restaurantResult = await restaurant.save();

        result.restaurant = restaurantResult._id;
        await result.save();

        return helpers.mergers.removePassword(restaurantResult._doc);
    },
    updateRestaurant: async (args, req) => {        
        const restaurantId = args.restaurantId;
        const restaurant = await Restaurant.findById(restaurantId);
        
        const userInput = args.restaurantInput;
        const {address, island, phone_number} = userInput;
        const {thumbnail, pictures, coverImage} = userInput;
        const {name} = userInput;
        const newContact = {address, island, phone_number};
        const newProfile = {thumbnail, pictures, coverImage};
        const newCreator = {name};

        helpers.cleaners.removeUndefined(newContact);
        helpers.cleaners.removeUndefined(newProfile);
        helpers.cleaners.removeUndefined(newCreator);
   
        await Contact.updateOne({_id: restaurant.contact}, newContact);        
        await User.updateOne({_id: restaurant.account}, newCreator);
        await Restaurant.updateOne({_id: restaurant._id}, newProfile);

        //console.log("changed",{...restaurant.contact._doc, ...oldContact});
        const restaurant2 = await Restaurant.findById(restaurantId).populate("account").populate("contact");
        //console.log(userInput);
        return restaurant2;

    }
};

module.exports = RestaurantResolvers;
