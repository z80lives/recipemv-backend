const auth = require("./auth");
const contact = require("./contact.gql");
const restaurant = require("./restaurant.gql");
const rootSchema = require('./schema.gql');
const recipeSchema = require('./recipe.gql');
const chefSchema = require('./chef.gql');

module.exports = [
    contact, 
    auth,
    restaurant,
    recipeSchema,
    chefSchema,
    rootSchema
];