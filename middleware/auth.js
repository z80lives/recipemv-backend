const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    const authHeader = req.get("Authorization");
    

    if(!authHeader){
        req.isAuth = false;
        return next();
    }
    const token = authHeader.split(" "); 
    
    if (!token || token === "" )
    {
        req.isAuth = false;
        return next();
    }

    let decodedToken;
    try{
        decodedToken =  jwt.verify(token[1], "1234");
    }catch(err){
        req.isAuth = false;
        console.log("authentication failed", err);
        return next();
    }

    req.isAuth = true;
    req.user_id = decodedToken.user_id;
    req.token = token[1];
    return next();
};